module Main exposing (..)

import Browser
import Html exposing (Html, button, div, text)
import Html.Events exposing (onClick)
import Random
import Time

init : String -> ( Model, Cmd MyMessages )
init name =
    let
        model : Model
        model =
            { value = 0
            , name = name
            , state = "init"
            , the_time = Time.millisToPosix 0
            }
    in
    ( model
    , Cmd.none
    )


subscriptions : Model -> Sub MyMessages
subscriptions m =
    Sub.batch 
        [ Time.every 1000 SetTime
        , Time.every 1000 (const TriggerRandom)
        ]


main : Program String Model MyMessages
main =
    -- init: flags -> (Model, Cmd MyMessages)
    Browser.element
        { init = init
        , subscriptions = subscriptions
        , update = update
        , view = view
        }


type MyMessages
    = Increment
    | Decrement
    | Reset
    | TriggerRandom
    | SetRandom Int -- (SetRandom : Int -> MyMessages)
    | SetTime Time.Posix -- (SetTime : Posix -> MyMessages)

type alias Model =
    { value : Int
    , name : String
    , state : String
    , the_time : Time.Posix
    }



const : a -> b -> a
const x _ = x



update : MyMessages -> Model -> ( Model, Cmd MyMessages )
update msg model =
    let

        make = \m fv -> { m | value = fv m.value }
        model_is = make model

    in
        case msg of
            Increment ->
                ( model_is ((+)1), Cmd.none )

            Decrement ->
                ( model_is (\v -> v - 1), Cmd.none )

            Reset ->
                ( model_is (const 0), Cmd.none )

            TriggerRandom ->
                ( { model | state = "triggered" }
                , Cmd.batch [ Random.generate SetRandom (Random.int 1 9999999999999999999999999) ]
                )

            SetRandom new_random_value ->
                ( { model | value = new_random_value, state = "done" }
                , Cmd.none 
                )

            SetTime theTime ->
                ( { model | the_time = theTime }
                , Cmd.none 
                )

view : Model -> Html MyMessages
view model =
    div []
        [ button [ onClick Decrement ] [ text "minus" ]
        , div [] [ text (String.fromInt model.value) ]
        , button [ onClick Increment ] [ text "plus" ]
        , button [ onClick Reset ] [ text "reset" ]
        , div [] [ text model.name, text " -> ", text model.state ]
        , div [] [ text (String.fromInt (Time.toSecond Time.utc model.the_time)) ]
        , button [ onClick TriggerRandom ] [ text "Random" ]
        ]
