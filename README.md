# Lets learn pure functional programming together!

- Web app - ELM 
	- build a webapp which clicking a button gets you a new chuck norris joke (https://api.chucknorris.io)
	- Drikus' Baseball Card Idea - Individual Profiles leading to Synthesis Resource Matching (https://miro.com/app/board/o9J_l4WONto=/)

## Some others topics

Languages
- PureScript
- Haskell
- F#
- OCaml / Bucklescript / ReasonML

Concepts
- Basics
	- "pure"
	- implicitly, strictly typed
	- lazy vs non-lazy
	- immutability
		- Does this mean I must "copy" everything? Isn't that wasteful/slow?
	- syntax, #, $, let and where, function signatures
	- curry / partial application
	- data, newtype, record - ADTs
	- pattern matching (function args, case, let)
	- Sum vs Product types
	- Type classes
	- Void vs Unit
	- Functional composition
	- Polymorphism (parametric - via typeclasses, e.g. "List a", row polymorphism also)


	

- Actually write some real code, using state, reader/writer, effect
- Lenses
- Functor / Applicative / Monad
- Category Theory



"I hope future generations will be as admiring of the programming skills
we’ve been displaying in building complex operating systems, web
servers, and the internet infrastructure. And, frankly, they should,
because we’ve done all this based on very flimsy theoretical foundations.
We have to fix those foundations if we want to move forward." - Bartosz Milewski

"In a strongly typed language, it’s enough to modify the declaration of
that function and then fix all the build breaks. In a weakly typed language,
the fact that a function now expects different data cannot be propagated to
call sites. Unit testing may catch some of the mismatches, but testing is
almost always a probabilistic rather than a deterministic process. Testing
is a poor substitute for proof." - Bartosz Milewski

